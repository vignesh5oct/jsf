package com.employee.database;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnect {
	public static Connection establishConnection() {
		Connection con = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = (Connection) DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","sys as sysdba", "1234");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return con;
	}
}
