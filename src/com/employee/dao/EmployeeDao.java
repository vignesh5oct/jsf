package com.employee.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.employee.model.Employee;


public interface EmployeeDao {
	ArrayList<Employee> getEmployeeListFromDb();
	ArrayList<Employee> search(String search);
	String save(Employee emp);
	String delete(int id) throws SQLException, ClassNotFoundException;
	String update(Employee emp) throws SQLException, ClassNotFoundException;
	String edit(int id);
}
