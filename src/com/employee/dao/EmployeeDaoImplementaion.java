package com.employee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

import javax.faces.context.FacesContext;

import com.employee.database.DbConnect;
import com.employee.model.Employee;


public class EmployeeDaoImplementaion implements EmployeeDao{
	
	public ArrayList<Employee> getEmployeeListFromDb() {
		ArrayList<Employee> employeeList = new ArrayList<Employee>();
		Connection con;
		Statement st;
		ResultSet rs;
		try {
			con = (Connection) DbConnect.establishConnection();
			st = con.createStatement();
			rs = st.executeQuery("select * from employee");
			while (rs.next()) {
				Employee emp = new Employee();
				emp.setId(rs.getInt("EMP_ID"));
				emp.setFirstname(rs.getString("First_Name"));
				emp.setLastname(rs.getString("Last_Name"));
				emp.setAge(rs.getInt("Age"));
				emp.setDesignation(rs.getString("Designation"));
				emp.setLocation(rs.getString("Location"));
				employeeList.add(emp);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return employeeList;
	}
	
	public ArrayList<Employee> search(String search) {
		ArrayList<Employee> employeeList = new ArrayList<Employee>();
		Connection con;
		Statement st;
		ResultSet rs;
		try {
			con = (Connection) DbConnect.establishConnection();
			st = con.createStatement();
			rs = st.executeQuery("select * from employee where first_name LIKE '%" + search + "%'");
			while (rs.next()) {
				Employee emp = new Employee();
				emp.setId(rs.getInt("EMP_ID"));
				emp.setFirstname(rs.getString("First_Name"));
				emp.setLastname(rs.getString("Last_Name"));
				emp.setAge(rs.getInt("Age"));
				emp.setDesignation(rs.getString("Designation"));
				emp.setLocation(rs.getString("Location"));
				employeeList.add(emp);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return employeeList;
	}

	@Override
	public String save(Employee emp) {
		int r = 0;
		String page = "";
		try {
			Connection con = (Connection) DbConnect.establishConnection();
			PreparedStatement pst = con.prepareStatement(
					"insert into employee(First_Name,Last_Name,Age,Designation,Location) values(?,?,?,?,?)");
			pst.setString(1, emp.getFirstname());
			pst.setString(2, emp.getLastname());
			pst.setInt(3, emp.getAge());
			pst.setString(4, emp.getDesignation());
			pst.setString(5, emp.getLocation());
			pst.execute();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (r != 0) {
			page = "createEmployee.xhtml?faces-redirect=true";
		} else {
			page = "main.xhtml?faces-redirect=true";
		}
		return page;
	}

	@Override
	public String delete(int id) throws ClassNotFoundException,SQLException {
		Connection con = (Connection) DbConnect.establishConnection();
		PreparedStatement pst = con.prepareStatement("delete from employee where EMP_ID=" + id);
		pst.executeUpdate();
		con.close();
		return "main.xhtml?faces-redirect=true";
		
	}

	@Override
	public String update(Employee emp) throws SQLException ,ClassNotFoundException{
		Connection con = (Connection) DbConnect.establishConnection();
		PreparedStatement pst = con.prepareStatement(
				"update employee set First_Name=?,Last_Name=?,Age=?,Designation=?,Location=? where EMP_ID=?");
		pst.setString(1, emp.getFirstname());
		pst.setString(2, emp.getLastname());
		pst.setInt(3, emp.getAge());
		pst.setString(4, emp.getDesignation());
		pst.setString(5, emp.getLocation());
		pst.setInt(6, emp.getId());
		pst.execute();
		con.close();
		return "main.xhtml?faces-redirect=true";
	}

	@Override
	public String edit(int id) {
		Employee emp = new Employee();
		Connection con;
		Statement st;
		ResultSet rs;
		Map<String, Object> sessionmap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		try {
			con = (Connection) DbConnect.establishConnection();
			st = con.createStatement();
			rs = st.executeQuery("select * from employee where EMP_ID=" + id);
			if (rs != null) {
				rs.next();
				emp.setId(rs.getInt("EMP_ID"));
				emp.setFirstname(rs.getString("First_Name"));
				emp.setLastname(rs.getString("Last_Name"));
				emp.setAge(rs.getInt("Age"));
				emp.setDesignation(rs.getString("Designation"));
				emp.setLocation(rs.getString("Location"));
			}
			sessionmap.put("editobj", emp);
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "editEmployee.xhtml?faces-redirect=true";
	}
}

	
