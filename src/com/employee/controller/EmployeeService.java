package com.employee.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import com.employee.model.Employee;


public interface EmployeeService {
	ArrayList<Employee> employeeList();
	ArrayList<Employee> searchEmployeeDetails(String search);
	String deleteEmployeeDetails(int id) throws SQLException, ClassNotFoundException;
	String editEmployeeDetails(int id);
	String updateEmployeeDetails(Employee emp) throws SQLException, ClassNotFoundException;
	String saveEmployeeDetails(Employee emp);
}
