package com.employee.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.employee.dao.EmployeeDao;
import com.employee.dao.EmployeeDaoImplementaion;
import com.employee.model.Employee;

@ManagedBean
@RequestScoped

public class EmployeeController implements EmployeeService{
	private ArrayList<Employee> employeeModel;
	EmployeeDao employeeDao = new EmployeeDaoImplementaion();
	
	@Override
	public ArrayList<Employee> employeeList(){
		employeeModel=employeeDao.getEmployeeListFromDb();
		return employeeModel;
	}

	@Override
	public ArrayList<Employee> searchEmployeeDetails(String search) {
		return employeeDao.search(search);
	}

	@Override
	public String editEmployeeDetails(int id) {
		return employeeDao.edit(id);
	}

	@Override
	public String updateEmployeeDetails(Employee emp) throws SQLException, ClassNotFoundException {
		return employeeDao.update(emp);
	}

	@Override
	public String deleteEmployeeDetails(int id) throws SQLException, ClassNotFoundException {
		/*removed comments */
		return employeeDao.delete(id);
		
	}

	@Override
	public String saveEmployeeDetails(Employee emp) {
		return employeeDao.save(emp);
	}
	
}
